﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject startMenu;
    public InputField usernameField;
    public GameObject votingCanvas;
    public Dropdown dropdown;
    public GameObject gameOverText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void ShowPlayerKicked()
    {
        StartCoroutine(UIManager.instance.ShowMessage(5));
    }

    public IEnumerator ShowMessage(float delay)
    {
        yield return new WaitForSeconds(delay);
        instance.votingCanvas.SetActive(false);
    }

    public void lockVote()
    {
        int pick = Int32.Parse(dropdown.options[dropdown.value].text.Split(' ')[0]);
        ClientSend.VotePick(pick);
    }

    public void ConnectToServer()
    {
        startMenu.SetActive(false);
        usernameField.interactable = false;
        Client.instance.ConnectToServer();
    }
}