﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class ClientHandle : MonoBehaviour
{
    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();

        Debug.Log($"Message from server: {_msg}");
        Client.instance.myId = _myId;
        ClientSend.WelcomeReceived();

        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();

        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation);
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.players[_id].transform.position = _position;
    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();

        GameManager.players[_id].transform.rotation = _rotation;
    }

    public static void Imposter(Packet packet)
    {
        int id = packet.ReadInt();
        bool isImposter = packet.ReadBool();

        GameManager.isImposter = isImposter;
    }

    public static void StartGame(Packet packet)
    {
        Vector3 position = packet.ReadVector3();
        Quaternion rotation = packet.ReadQuaternion();

        for (int i = 1; i <= GameManager.players.Count; i++)
        {
            GameManager.players[i].transform.position = position;
            GameManager.players[i].transform.rotation = rotation;
        }
        
    }

    public static void kickedPlayer(Packet packet)
    {
        int id = packet.ReadInt();
        string username = packet.ReadString();

        Voting.playerKicked(id, username);
    }

    public static void vote(Packet packet)
    {
        List<int> deadPlayersId = packet.ReadIntList();
        List<string> deadPlayersUsernames = packet.ReadStringList();

        Debug.LogError("VOTING");

        GameManager.deadPlayers = createDeadPlayersDictionary(deadPlayersId, deadPlayersUsernames);
        Voting.startVote();
    }

    public static void GameOver(Packet packet)
    {
        UIManager.instance.gameOverText.SetActive(true);
        UIManager.instance.gameOverText.GetComponent<Text>().text = "GAME OVER\nIMPOSTER " + packet.ReadString();
    }

    private static Dictionary<int, string> createDeadPlayersDictionary(List<int> deadPlayersId, List<string> deadPlayersUsernames)
    {
        Dictionary<int, string> dictionary = new Dictionary<int, string>();
        
        for(int i = 0; i < deadPlayersId.Count; i++)
        {
            dictionary.Add(deadPlayersId[i], deadPlayersUsernames[i]);
        }

        return dictionary;
    }
}