﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Voting : MonoBehaviour
{

    public static void startVote()
    {
        UIManager.instance.votingCanvas.SetActive(true);
        Dropdown dropdown = GameObject.Find("UsernameDropdown").GetComponent<Dropdown>();
        UIManager.instance.dropdown = dropdown;
        dropdown.ClearOptions();
        dropdown.AddOptions(returnUsernameList());
    }

    public static void playerKicked(int id, string username)
    {
        GameObject.Find("VotingText").GetComponent<Text>().text = username + " was kicked.";
        UIManager.instance.ShowPlayerKicked();
    }

    private static List<string> returnUsernameList()
    {
        List<string> usernames = new List<string>();
        for (int i = 1; i <= GameManager.players.Count; i++)
        {
            if(!GameManager.deadPlayers.ContainsKey(i))
            {
                usernames.Add(i + " " + GameManager.players[i].username);
            }
            
        }
        return usernames;
    }
}
